# Cordova/React application template

## Tutorial for creating a react application with cordova
https://medium.com/@pshubham/using-react-with-cordova-f235de698cc3

## Get files

Clone this repository

```bash
git clone https://gitlab.com/DimEducation/react-cordova.git
```

## Add platform

```bash
cordova platform add browser
```

or

```bash
cordova platform add browser
```

## Run

For running the application, run the cordova application juste as all other cordova application

```bash
cordova run browser
```

or

```bash
cordova run android
```

## Plugins
 

```bash
cordova plugin add cordova-plugin
```


[Run result](./run-result.png)
